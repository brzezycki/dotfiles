  ____    _    ____   _____           ____  _____  ____   ___  
 | __ )  / \  |  _ \| ____|         |  _ \| ____|  _ \ / _ \ 
 |  _ \ / _ \ | |_) |  _|    _____  | |_) |  _| | |_) | | | |
 | |_) / ___ \|  _ <| |___  |_____| |  _ <| |___|  __/| |_| |
 |____/_/   \_\_| \_\_____|         |_| \_\_____|_|    \___/ 
                                                             

mkdir dotfiles in $HOME

cd into &
        >>  git init -bare

--------------------------

create alias 

```
# gitlab bare-repo
alias config='/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'
```


--------------------------

USUAL USE:

    `config add [PATH to file]`		        # add files to bare repo

    `config commit -m "Initial commit"`		# commit all changes

    `config push`			        # upload to repo




