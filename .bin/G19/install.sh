#!/bin/bash

# Update

# Requirements
echo "Installing requirements..."
sudo pacman -Syu install -y git gcc make autoconf automake build-essential libtool python-gtk2-dev python-virtkey python-pyinotify python-usb python-rsvg python-xlib python-keyring python-pip python-lxml python-gconf python-wnck
sudo pip install pillow python-uinput
echo "Installing requirements completed."
echo ""

# Install gnome15
echo "Installing gnome15..."
cd /tmp
git clone https://github.com/Huskynarr/gnome15.git
cd gnome15
libtoolize
aclocal
autoconf
automake --add-missing
./configure
make
sudo make install
echo "Installing gnome15 completed."
echo ""
