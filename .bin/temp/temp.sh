#    TEMP

#!/bin/bash

while :
do
	clear
	sensors | grep Tctl  > ~/.bin/temp/heat.log
	echo ""
	printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' -
	echo "[SYSTEM HEAT:]"
	cut -d: -f2 ~/.bin/temp/heat.log | figlet -f banner 	# CUT -d seperator zeichen : | -f bestimmtes Feld  
	printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' -
	
	sleep 1

done
