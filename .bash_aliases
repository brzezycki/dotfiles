################################################################
#  __  __            _    _     ___    _    ____  _____ ____   #
# |  \/  |_   _     / \  | |   |_ _|  / \  / ___|| ____/ ___|  #
# | |\/| | | | |   / _ \ | |    | |  / _ \ \___ \|  _| \___ \  #
# | |  | | |_| |  / ___ \| |___ | | / ___ \ ___) | |___ ___) | #
# |_|  |_|\__, | /_/   \_\_____|___/_/   \_\____/|_____|____/  #
#         |___/                                                #
################################################################

## apt
alias ai="sudo apt install"
alias as="sudo apt search"
alias au="sudo apt update"
alias auu="sudo apt update && sudo apt upgrade"

## pacman
alias pm-install="sudo pacman -S"		# install
alias pm-update='sudo pacman -Syy'		# update
alias pm-upgrade="sudo pacman -Syyu"	# upgrade
alias pm-remove="sudo pacman -R"		# remove
alias pm-search="sudo pacman -Ss"		# search
alias pm-mirror='sudo pacman-mirror -g' # mirror
alias pm-list='sudo pacman -Qq'			# list installed

## gitlab bare-repo
alias config='/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'

## SYSTEM
alias df="df -h"
alias cp="cp -i"
alias mv='mv -i'
alias rm='rm -i'
alias free='free -m'
alias mount='mount | column -t'
alias wget='wget -c'

## SHUTDOWN
alias ssn="sudo shutdown now"
alias sr="sudo reboot"


## cd
alias ..='cd ..' 
alias ...='cd ../..'
alias .3='cd ../../..'
alias .4='cd ../../../..'
alias .5='cd ../../../../..'


## ls
alias ls='lsd --group-dirs first'


## SHORTCUTS
alias bash-r="source ~/.bashrc"    # reload .bashrc
alias kde-r="bash ~/.bin/restartPLASMA/main.bash"    # easy kde refresh
alias ali="micro ~/.bash_aliases"
alias bashrc="micro ~/.bashrc"
alias netspeed="sudo speedometer -r wlx74da38ea6c5e -t wlx74da38ea6c5e"

## MY TOOLS
alias temp="bash ~/.bin/temp/temp.sh"
alias hostscan="bash ~/.bin/hostscan/scan.sh"


## PI
alias pi="ssh pi@192.168.178.22"

## rg
alias rg="rg -n -i"		# display line numbers + ignore case sensitive
